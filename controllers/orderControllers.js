const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createOrder = (data) =>{

    return User.findById(data.userId).then((result) => {

    if (!result.isAdmin){

    return Product.findById(data.productId).then((result, err) => {

            let newOrder = new Order({
                userId: data.userId,
                products: [{
                    productId: data.productId,
                    quantity: data.quantity,
            }],
                totalAmount: data.quantity * result.price
     })

    return newOrder.save().then((order,error)=>{
            if(error){
                    return false;
            } else {

            	let productOrders = {
            		orders: [{
            			orderId: data.userId,
            			quantity: data.quantity
            		}]
            	}

            	return Product.findByIdAndUpdate(data.productId, {$push: productOrders}).then((order,error)=>{

                            if(error){
                                return false;
                            } else {
                                return true; 

            		}
            	})

                
            }
        })

})

    } else {
    	return false;
}
})
}


module.exports.getAllOrders = (data) => {
	return User.findOne({_id : data.id}).then(result => {
		if(result == null){
			return false
		}
		else{
			if(data.isAdmin){
				return Order.find().then((result, error) => {
					if(error){
						return false
					}
					else if(result == null){
						return false
					}
					else{
						return result
					}
				})
			}
			else{
				return false
			}
		}
	})
}