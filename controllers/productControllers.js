const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Product creation
module.exports.addProduct = (data) => {
	if (data) {
	let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	});
		return newProduct.save().then((product, error) => {
		if (error) {
			return false;
		} else {
			return "Product Successfully created", product;
		};
	});
	}else {
		return false;
	}
};

// Search active Product

module.exports.searchActiveProduct = () =>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	})
};

// Search specific product

module.exports.findSpecificProduct = (findId) => {
	return Product.findById(findId.productId).then(item=>{
		return item;
	})
}

// Search all product
module.exports.findAllProduct = () => {
	return Product.find({}).then(items=>{
		return items;
	})
}

// Update product controller

module.exports.reviseProduct = (findId,reqBody) =>{
let updatedProduct = {
	name : reqBody.name,
	description : reqBody.description,
	price : reqBody.price
};
	return Product.findByIdAndUpdate(findId.productId,updatedProduct).then((product,error)=>{
		if(error){
			return false;
		}else {
			return product;
		};
	});
};


// Archive Product
module.exports.archiveProductAsAdmin = (findId) => {
	let archiveProduct = {
		isActive : false
	};
	return Product.findByIdAndUpdate(findId.productId,archiveProduct).then((product,error) => {
		if(error){
			return false;
		}else{
			return product;
		};
	});
};


// activate product
module.exports.activateProductAsAdmin = (findId) => {
	let activateProduct = {
		isActive : true
	};
	return Product.findByIdAndUpdate(findId.productId,activateProduct).then((product,error) => {
		if(error){
			return false;
		}else{
			return product;
		};
	});
};
