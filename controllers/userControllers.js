const User = require("../models/User");
const Product = require("../models/Product");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// register User
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10) //reqBody.password
})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

// Login user
module.exports.AuthenticateUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(!result){
			return false;
		}else{
			const checkPassword = bcrypt.compareSync(reqBody.password,result.password);
			if(checkPassword){
				return { access: auth.createAccessToken(result)}
			}else{
				return false;
			};
		};
	});
};

// update user as admin
module.exports.updateUserAsAdmin = (reqParams) =>{
	let updateUser = 
	{
		isAdmin : true
	};
	return User.findByIdAndUpdate(reqParams.id,updateUser).then((user,error)=>{
		if(error){
			return false;
		}else {
			return user
		};
	});
};

// Get user details
module.exports.getUsersDetails = (details) => {

	return User.findById(details.userId).then((user,error)=>{
		if (error){
			return false
		}else{
			return user
		}
	})
};

module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};

// Create order

// module.exports.createOrder = async (data) =>{
	
// 	let getUserAndUpdate = await User.findById(data.userId).then(user=>{
		
// 		user.order.push({productId:data.productId});

// 		return user.save().then((user,error)=>{
// 			if(error){
// 				return false;
// 			}else{
// 				return user;
// 			};
// 		});
// 	});

// 	let getProductAndUpdate = await Product.findById(data.productId).then(product=>{
		
// 		product.orders.push({userId:data.userId});

// 		return product.save().then((products,error)=>{
// 			if(error){
// 				return false;
// 			}else{
// 				return products;
// 			};
// 		});
// 	});
// 	if(getUserAndUpdate&&getProductAndUpdate){
// 		return getUserAndUpdate&&getProductAndUpdate;
// 	}else{
// 		return false;
// 	};
// };
