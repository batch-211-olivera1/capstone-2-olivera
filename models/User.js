const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
	firstName : {
		type:String, 
		required:true
	},

	lastName : {
		type:String, 
		required:true
	},

	email : {
		type:String, 
		required : true
	},

	password : {
		type:String, 
		required:true
	},

	mobileNo : {
		type:String, 
		required : true
	},

	isAdmin : {
		type:Boolean, 
		default:false
	},
	// order : [
	// {
	// 	totalAmount : {
	// 		type:Number,
	// 		required:false
	// 	},
	// 	products : 
	// 	[{
	// 		productId: {
	// 			type:String,
	// 			required:true},
	// 		quantity: {
	// 			type:Number,
	// 			default:1
	// 		}
	// 	}],
	// 	purchasedOn : {
	// 		type:Date,
	// 		default:Date.now()
	// 	}
	// }
	// 	]
});

module.exports = mongoose.model('User', userSchema)