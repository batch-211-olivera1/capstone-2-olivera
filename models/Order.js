const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema({
	userId : {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
		},
	totalAmount : {
		type: Number,
		required:true
		},
	purchasedOn : {
		type: Date,
		default: Date.now()
		},	 

 	products : [
 	{
		productId : {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Product',
			required: true
		},
		quantity : {
			type: Number,
			default: 1
		}
	}
	]
})

module.exports = mongoose.model("Order",orderSchema);
