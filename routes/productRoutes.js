const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");


// Product creation route
router.post("/newProduct",auth.verify,(req,res)=>{
	const newProduct = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(newProduct).then(resultFromController=>res.send(resultFromController));
});

// Search all Product route
router.get("/activeProducts",(req,res)=>{
	productController.searchActiveProduct(req.body).then(resultFromController=>res.send(resultFromController));
});

// Search all product

router.get("/search/all", (req,res) => {
	productController.findAllProduct().then(resultFromController=>res.send(resultFromController));
});

// Search specific product route

router.get("/search/:productId", (req,res) => {
	productController.findSpecificProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

// Update product route

router.put("/update/:productId",auth.verify,(req,res) => {
	productController.reviseProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

// Archive product reoute
router.put("/archive/:productId",auth.verify,(req,res) => {
	productController.archiveProductAsAdmin(req.params).then(resultFromController=>res.send(resultFromController));
});

// Activate product route
router.put("/activate/:productId",auth.verify,(req,res) => {
	productController.activateProductAsAdmin(req.params).then(resultFromController=>res.send(resultFromController));
});

module.exports = router;
