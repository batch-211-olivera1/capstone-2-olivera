const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");

const auth = require("../auth");

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

router.post("/login",(req,res)=>{
	userController.AuthenticateUser(req.body).then(resultFromController=>res.send(resultFromController));
});

router.put("/updateAsAdmin/:id",auth.verify,(req,res)=>{

	userController.updateUserAsAdmin(req.params).then(resultFromController=>res.send(resultFromController));
});

router.get("/details",auth.verify,(req,res)=>{

	const userDetails = auth.decode(req.headers.authorization);

	userController.getUsersDetails({userId:userDetails.id}).then(resultFromController=>res.send(resultFromController));
});

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


// router.post("/checkout",auth.verify, (req, res) => {

// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,	
// 		totalAmount: req.body.totalAmount,
// 		productId: req.body.productId,
// 		quantity: req.body.quantity			
// 	}
// 	userController.createOrder(data).then(resultFromController=> res.send(resultFromController));
// })

module.exports = router;

