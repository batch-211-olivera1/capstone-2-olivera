const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");


router.post("/checkout",auth.verify,(req,res)=>{
    const data = {
            userId: auth.decode(req.headers.authorization).id,
            productId: req.body.productId,
            quantity: req.body.quantity,
            
        }
        console.log(data)
    orderController.createOrder(data).then(resultFromController=>res.send(resultFromController));
    
});

router.get("/getOrder", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization)
	
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;